# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets, Qt
from PyQt5.QtWidgets import QDialog, QPushButton, QWidget, QVBoxLayout, QHBoxLayout, QLabel
from PyQt5.Qt import *

import qss_dark


class BaseDialog(QWidget):
    def __init__(self, parent, title="提示信息", content="", okLabel="确定", cancleLabel="取消", width=100, height=100):
        super().__init__(parent)
        self.parentWindow = parent
        self.title = title
        self.content = content
        self.okLabel = okLabel
        self.cancleLabel = cancleLabel
        self.width = width
        self.height = height
        self.setStyleSheet(self.parentWindow.styleSheet())

    def show(self):
        vbox = QVBoxLayout()  # 纵向布局
        hbox = QHBoxLayout()  # 横向布局
        panel = QLabel()
        panel.setObjectName("dialogContent")
        panel.setAlignment(Qt.AlignTop | Qt.AlignLeft)
        panel.setText(self.content)
        self.dialog = QDialog()
        self.dialog.resize(self.width, self.height)
        self.dialog.setWindowFlags(QtCore.Qt.WindowCloseButtonHint)
        self.dialog.setWindowIcon(self.parentWindow.windowIcon())
        self.dialog.setWindowTitle(self.title if self.title != "" else "提示信息")
        self.dialog.setStyleSheet(self.parentWindow.styleSheet())
        self.okBtn = QPushButton(self.okLabel if self.okLabel != "" else "确定")
        self.okBtn.setStyleSheet(qss_dark.buttonPrimary)
        self.okBtn.setFocus()
        self.cancelBtn = QPushButton(self.cancleLabel if self.cancleLabel != "" else "取消")

        # 绑定事件
        self.okBtn.clicked.connect(self.ok)
        self.cancelBtn.clicked.connect(self.cancel)

        vbox.setContentsMargins(0, 0, 0, 0)
        vbox.setSpacing(0)
        # 确定与取消按钮横向布局
        hbox.addWidget(self.okBtn)
        hbox.addWidget(self.cancelBtn)
        hbox.setAlignment(Qt.AlignRight)
        buttonsWidget = QWidget()
        buttonsWidget.setObjectName("dialogFooter")
        buttonsWidget.setLayout(hbox)
        buttonsWidget.setFixedHeight(60)
        # buttonsWidget.setStyleSheet("")

        # 消息label与按钮组合纵向布局
        vbox.addWidget(panel)
        vbox.addWidget(buttonsWidget)
        self.dialog.setLayout(vbox)

        self.dialog.setWindowModality(QtCore.Qt.ApplicationModal)  # 该模式下，只有该dialog关闭，才可以关闭父界面
        self.dialog.exec_()

    def setTitle(self, title):
        self.title = title

    def setContent(self, content):
        self.content = content

    def setOkLabel(self, text):
        self.okLabel = text

    def setCancleLabel(self, text):
        self.cancleLabel = text

    def ok(self):
        self.dialog.close()

    def cancel(self):
        self.dialog.close()

