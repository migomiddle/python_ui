# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets, Qt
from PyQt5.QtWidgets import QDialog, QPushButton, QWidget, QVBoxLayout, QHBoxLayout, QLabel

from dialog import BaseDialog


class AboutDialog(BaseDialog):
    def __init__(self, parent):
        super().__init__(parent, title="关于我们", width=500, height=200)
        self.setContent("关于我们的内容区域")

