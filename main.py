
import qdarkstyle
from PyQt5 import QtWidgets, Qt, QtGui

import app
import sys

if __name__ == '__main__':
    win = QtWidgets.QApplication(sys.argv)
    MainWindow = app.Ui_MainWindow() # QtWidgets.QMainWindow()
    # setup stylesheet
    # MainWindow.setStyleSheet(qdarkstyle.load_stylesheet())
    MainWindow.showMaximized()
    sys.exit(win.exec_())
