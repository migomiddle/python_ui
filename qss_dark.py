globalStyle = '''
*{font: 9pt "微软雅黑"; color: #CCCCCC;}
QMainWindow{background:#3C3F41;padding:0px;margin:0px;}
QMenuBar {background:#3C3F41;color:#CCCCCC; font-size:12pt; border-bottom:2px solid #2B2B2B;margin:0px 10px 0px 10px;}
QMenuBar::item{padding:10px;margin:0px 20px 0px 0px;}
QMenuBar::item:selected{background:#4B6EAF;}
QMenu{background:#3C3F41;color:#CCCCCC; font-size:12pt;padding:0px;margin:0px;border:1px solid #515151;}
QMenu::item{padding:10px;min-width:150px;}
QMenu::icon{margin-left:10px; margin-right:10px;}
QMenu::item:pressed{background:#4B6EAF;}
QMenu::item:selected{background:#4B6EAF;}
QTreeView{background:#3C3F41;padding: 10 0px;font-size:10pt;color:#CCCCCC;border-top:5px solid #3E86A0;}
QTreeView::item{font-size:10pt;color:#CCCCCC;margin-bottom:5px;}
QTreeView::item:hover{background:#3C3F41;}
QTreeView::item:selected:active{background:#4B6EAF;}
QTreeView::item:selected:!active{background:#0D293E;}
QTreeView::branch {color:white;}
QFrame{background:#2B2B2B;}
QStatusBar{background:#3C3F41;}
QSplitter{padding:0px;margin:0px;}
QSplitter::handle{background:#3C3F41;spacing:0px;padding:1px;margin:0px;}
QGroupBox{border:2px solid #495B61;background:#3C3F41;border-radius: 6px; font-size:10pt; font-weight:bold;margin-top: 1ex;}
QGroupBox::title{color:#3E86A0;subcontrol-origin: margin;
    subcontrol-position: top center; /* position at the top center */
    padding: 0 3px;
    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                      stop: 0 #FFOECE, stop: 1 #FFFFFF);}
QGroupBox QLabel:hover{border:5px solid #3E86A0;}
QPushButton{color:#CCCCCC;border:1px solid #5E6060;padding:6px;border-radius: 10px;}
QPushButton:hover{font-weight:bold;color:white;background:#3C3F41;}
QPushButton:pressed{border-width:2px;}
QPushButton:disabled{background:gray;}
QScrollArea{margin:0px;border:none;}
QStatusBar {
background: #2B2B2B;
height:20px;
padding:0px;
margin:0px;
}

QStatusBar::item {
  border: none;
}

QStatusBar QToolTip {
  background-color: #1A72BB;
  border: 1px solid #19232D;
  color: #19232D;
  padding: 0px;
  opacity: 230;
}

QStatusBar QLabel {
  background: transparent;
  color:#7C7A7A;
}
#dialogFooter{background:#3C3F41;padding:0px;}
#dialogFooter QPushButton{min-width:100px;}
#dialogContent{background:black;padding:5px;color:white;}
'''

backgroundTransparent = '''QWidget{background:transparent;}'''
buttonPrimary = '''QPushButton{background:#365880;font-weight:bold;} QPushButton:hover{background:#406086;}'''
buttonDanger = '''QPushButton{background:#BA1339;font-weight:bold;} QPushButton:hover{background:#D03055;}'''