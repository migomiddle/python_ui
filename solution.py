# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QSize
from PyQt5.QtGui import QFocusEvent
from PyQt5.QtWidgets import QWidget, QScrollArea, QLayout

import qss_dark


class SolutionWidget(QWidget):
    def __init__(self):
        super().__init__()
        self.setStyleSheet(qss_dark.globalStyle + qss_dark.backgroundTransparent)
        self.layout = QtWidgets.QGridLayout()
        self.layout.setContentsMargins(0, 0, 10, 10)
        self.layout.setSpacing(10)
        self.layout.setVerticalSpacing(20)
        # self.setLayout(self.layout)
        w = QWidget()
        w.setLayout(self.layout)
        self.scroll = QScrollArea()
        self.scroll.setMinimumSize(400, 400)
        self.scroll.setAlignment(QtCore.Qt.AlignTop)
        self.scroll.setWidget(w)
        self.scroll.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.scroll.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.scroll.setWidgetResizable(True)
        la = QtWidgets.QHBoxLayout()
        la.addWidget(self.scroll)
        self.setLayout(la)

        positions = [(i, j) for i in range(2) for j in range(3)]
        n = 0
        # for i in range(1, 7):
        for position in positions:
            print(position)
            n = n + 1
            if n > 3:
                n = 1
            __widget = self._createItemBox(f"images/{n}.jfif", f"拍摄方案-{n+position[0]+position[1]}", "高速摄影机械臂是此类机械臂中移速最快者——所捕捉的清晰对焦图像是手持或任何其他方式都无法实现的。不论是拍摄广告片、舞台秀、电影或是电视剧，都能为影像增添全新视感；机械臂从完全静止到高速运行，再回到完全静止，整个过程不到一秒，它能精确地捕捉到最快速的动作。水平和垂直运动速度都能达到2米每秒，可在1秒钟内选择半圈，几乎能与任何运动保持同步。")
            if position[0] == 0 and position[1] == 0:
                __widget.setFocus()
            self.layout.addWidget(__widget, position[0], position[1])
            # self.layout.setRowMinimumHeight(position[0], 1000)

        QtCore.QMetaObject.connectSlotsByName(self)

    def _createItemBox(self, img, title, description):
        groupBox = QtWidgets.QGroupBox(self)
        groupBox.setFixedHeight(700)
        groupBox.setTitle(title)
        groupBox_layout = QtWidgets.QVBoxLayout(groupBox)
        label = QtWidgets.QLabel(groupBox)
        label.setStyleSheet("margin-top:20px;background:black;")
        label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        jpg = QtGui.QPixmap(img).scaled(400, 300)
        label.setPixmap(jpg)
        label.setFixedHeight(300)
        groupBox_layout.addWidget(label)
        label_2 = QtWidgets.QLabel(groupBox)
        label_2.setTextFormat(QtCore.Qt.RichText)
        label_2.setText(description)
        label_2.setWordWrap(True)
        label_2.setAlignment(QtCore.Qt.AlignTop)
        label_2.setStyleSheet("padding:5px;background:black;color:#43D9D9;")
        label_2.setFixedHeight(300)
        groupBox_layout.addWidget(label_2)
        buttons_widget = QtWidgets.QWidget()
        buttons_widget.setFixedHeight(60)
        buttons_layout = QtWidgets.QHBoxLayout(buttons_widget)
        pushButton = QtWidgets.QPushButton(groupBox)
        pushButton.setText("使用")
        pushButton.setStyleSheet(qss_dark.buttonPrimary)
        # pushButton.setDisabled(True)
        buttons_layout.addWidget(pushButton)
        pushButton2 = QtWidgets.QPushButton(groupBox)
        pushButton2.setText("参数")
        buttons_layout.addWidget(pushButton2)
        buttons_widget.setLayout(buttons_layout)
        groupBox_layout.addWidget(buttons_widget)
        return groupBox
